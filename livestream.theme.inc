<?php

/**
 * Theme function for large video embed.
 */
function theme_livestream_large($variables) {
  $field = '<iframe width="620" height="349" src="http://cdn.livestream.com/embed/connectedlearningtv?layout=4&clip=';
  $field .= $variables['clip_id'];
  $field .= '&color=0xe7e7e7&autoPlay=false&mute=false&iconColorOver=0x888888&iconColor=0x777777&allowchat=true&height=385&width=640" style="border:0;outline:0" frameborder="0" scrolling="no"></iframe>';
  return $field;
}


/**
 * Theme function for large video embed.
 */
function theme_livestream_thumb($variables) {
  $field = '<iframe width="210" height="118" src="http://cdn.livestream.com/embed/connectedlearningtv?layout=4&clip=';
  $field .= $variables['clip_id'];
  $field .= '&color=0xe7e7e7&autoPlay=false&mute=false&iconColorOver=0x888888&iconColor=0x777777&allowchat=true&height=385&width=640" style="border:0;outline:0" frameborder="0" scrolling="no"></iframe>';
  return $field;
}
